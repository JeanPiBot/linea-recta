package main

import "fmt"

func main() {
	var x1 int
	var y1 int
	var x2 int
	var y2 int

	fmt.Println("Escriba la coordenada de X1")
	fmt.Scanln(&x1)

	fmt.Println("Escriba la coordenada de Y1")
	fmt.Scanln(&y1)

	fmt.Println("Escriba la coordenada de X2")
	fmt.Scanln(&x2)

	fmt.Println("Escriba la coordenada de Y2")
	fmt.Scanln(&y2)

	m := (y1 - y2) / (x1 - x2)

	if m != 0 {
		fmt.Printf("m, (x- , x1) + , y1 \n")
		if m > 0 {
			fmt.Println("La pendiente de la recta es positiva y el resultado es ", m)
		} else {
			fmt.Println("La pendiente de la recta es negativa y el resultado es ", m)
		}

	} else {
		fmt.Printf("El valor es %d", y1)
	}

}
